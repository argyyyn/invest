import {HTTP} from '../http-common'
const URI = 'api/news/'

class News {
    static all (then, catcher, params = null) {
        return HTTP.get(URI + 'getAll', { params: params })
            .then(({data}) => then(data))
            .catch((errors) => catcher(errors.response))
    }

    static show (then, catcher, id, params = null) {
        return HTTP.get(URI + 'get/' + id, { params })
            .then(({data}) => then(data))
            .catch((errors) => catcher(errors))
    }

    static post (then, catcher, params) {
        return HTTP.post(URI + 'save', params)
            .then(({data}) => then(data))
            .catch((errors) => catcher(errors))
    }

    static save (then, catcher, params) {
        return HTTP.put(URI + 'save', params)
            .then(({data}) => then(data))
            .catch((errors) => catcher(errors))
    }

    static delete (then, catcher, params) {
        return HTTP.delete(URI + 'delete/' + params)
            .then(({data}) => then(data))
            .catch((errors) => catcher(errors))
    }

}
export default News

