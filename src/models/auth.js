import {HTTP} from '../http-common'
const URI = 'user/'

class Auth {
    static register (then, catcher, params = null) {
        return HTTP.post(URI + '/register', params)
            .then(({data}) => then(data))
            .catch((errors) => catcher(errors.response))
    }

    static login (then, catcher, params = null) {
        return HTTP.post(URI + 'signin', params)
            .then(({data}) => then(data))
            .catch((errors) => catcher(errors.response))
    }

    static me (then, catcher, params = null) {
        return HTTP.get(URI + 'me', { params })
            .then(({data}) => then(data))
            .catch((errors) => catcher(errors))
    }

    static save (then, catcher, params = null) {
        return HTTP.post(URI + 'save', params)
            .then(({data}) => then(data))
            .catch((errors) => catcher(errors))
    }

    static role (then, catcher, params = null) {
        return HTTP.get('/role', params)
            .then(({data}) => then(data))
            .catch((errors) => catcher(errors))
    }

    static password (then, catcher, params = null) {
        return HTTP.post(URI + 'change', params)
            .then(({data}) => then(data))
            .catch((errors) => catcher(errors))
    }

}
export default Auth
