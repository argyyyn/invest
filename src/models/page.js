import {HTTP} from '../http-common'
const URI = 'api/page/'

class Page {
    static get (then, catcher, params = null) {
        return HTTP.get(URI + 'get', { params: params })
            .then(({data}) => then(data))
            .catch((errors) => catcher(errors.response))
    }

    static post (then, catcher, data, params = null) {
        return HTTP.post(URI + 'save', data, { params: params})
            .then(({data}) => then(data))
            .catch((errors) => catcher(errors.response))
    }

    static put (then, catcher, data, params = null) {
        return HTTP.put(URI + 'save', data, { params: params})
            .then(({data}) => then(data))
            .catch((errors) => catcher(errors.response))
    }

}
export default Page

