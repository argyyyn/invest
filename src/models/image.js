import {HTTP} from '../http-common'
const URI = 'file-system/image'

class Image {
    static get (then, catcher, id, params = null) {
        return HTTP.get('http://89.219.20.138:16801/file-system/image/9738bc83-40ea-41d7-bf29-cd02169516e6', { params: params })
            .then(({data}) => then(data))
            .catch((errors) => catcher(errors.response))
    }

    static post (then, catcher, image = null) {
        return HTTP.post(URI, image, {
            headers: {
                'Content-Type': 'multipart/form-data'
            }
        })
            .then(({data}) => then(data))
            .catch((errors) => catcher(errors.response))
    }



}
export default Image

