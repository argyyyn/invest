import axios from 'axios'

export const HTTP = axios.create({
    baseURL: 'http://89.219.20.138:16801/',
    headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
    }
})
