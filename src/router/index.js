import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'

Vue.use(VueRouter)

function requireAuth (to, from, next) {
  if (localStorage.token) {
    next()
  } else {
    next({
      name: 'Home'
    })
  }
}

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/borrower',
    name: 'Borrower',
    component: () => import('../views/Borrower.vue')
  },
  {
    path: '/blog',
    name: 'Blog',
    component: () => import('../views/Blog.vue')
  },
  {
    path: '/blog/:id',
    name: 'BlogSingle',
    component: () => import('../views/BlogSingle.vue')
  },
  {
    path: '/about/',
    name: 'About',
    component: () => import('../views/About.vue')
  },
  {
    path: '/contacts/',
    name: 'Contacts',
    component: () => import('../views/Contacts.vue')
  },
  {
    path: '/faq/',
    name: 'FAQ',
    component: () => import('../views/FAQ.vue')
  },
  {
    path: '/investment',
    name: 'Investment',
    component: () => import('../views/Investment.vue')
  },
  {
    path: '/auth/',
    name: 'Auth',
    component: () => import('../views/Auth.vue')
  },
  {
    path: '/admin',
    name: 'Admin',
    beforeEnter: requireAuth,
    component: () => import('../views/Admin/Index.vue'),
    children: [
      {
        path: 'contacts-old',
        name: 'AdminContactsOld',
        component: () => import('../views/Admin/ContactsOld.vue')
      },
      {
        path: 'contacts',
        name: 'AdminContacts',
        component: () => import('../views/Admin/Contacts.vue')
      },
      {
        path: 'main',
        name: 'AdminMain',
        component: () => import('../views/Admin/Main.vue')
      },
      {
        path: 'investment',
        name: 'AdminInvestment',
        component: () => import('../views/Admin/Investment.vue')
      },
      {
        path: 'borrower',
        name: 'AdminBorrower',
        component: () => import('../views/Admin/Borrower.vue')
      },
      {
        path: 'about',
        name: 'AdminAbout',
        component: () => import('../views/Admin/About.vue')
      },
      {
        path: 'faq',
        name: 'AdminFAQ',
        component: () => import('../views/Admin/FAQ.vue')
      },
      {
        path: 'documents',
        name: 'AdminDocuments',
        component: () => import('../views/Admin/Documents.vue')
      },
      {
        path: 'articles',
        name: 'AdminArticles',
        component: () => import('../views/Admin/Articles.vue')
      },
      {
        path: 'profile',
        name: 'AdminProfile',
        component: () => import('../views/Admin/Profile.vue')
      },
      {
        path: 'article/:id',
        name: 'AdminArticle',
        component: () => import('../views/Admin/Article.vue')
      },
      {
        path: 'article/',
        name: 'AdminArticleNew',
        component: () => import('../views/Admin/Article.vue')
      },
      {
        path: 'help',
        name: 'AdminHelp',
        component: () => import('../views/Admin/Help.vue')
      },
      {
        path: 'request/:id',
        name: 'AdminRequest',
        component: () => import('../views/Admin/Request.vue')
      },
      {
        path: 'requests',
        name: 'AdminRequests',
        component: () => import('../views/Admin/Requests.vue')
      },
    ]
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
