import Vue from 'vue'
import Vuex from 'vuex'
import locale from './modules/locale'
// import menus from './modules/menu'
import auth from './modules/auth'
Vue.use(Vuex)

export const store = new Vuex.Store({
modules: {
  locale,
  auth,
  // menus
}})
