import i18n from '../../locales/index'
import { HTTP } from '../../http-common'

const state = {
    lang: i18n.locale
}

const getters = {
    getLang: state => () => state.lang
}

const mutations = {
    SET_LANG (state, lang) {
        state.lang = lang
    }
}

const actions = {
    SET_LANG: ({commit}, lang) => {
        i18n.locale = lang
        HTTP.defaults.headers.common['Accept-Language'] = lang
        commit('SET_LANG', lang)
    }
}

export default {
    state,
    getters,
    actions,
    mutations
}
