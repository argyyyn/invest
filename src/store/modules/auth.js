import Auth from '../../models/auth'
import { HTTP } from '../../http-common'
import router from "../../router";

const state = {
    user: null
}

const getters = {
    getUser: state => () => state.user,
    isAuthenticated: state => () => !!state.user
}

const mutations = {
    LOG_IN (state, user) {
        state.user = user
    },
    LOG_OUT (state) {
        state.user = null
    }
}

const actions = {
    SET_USER: ({commit}, data = null) => {
        let user = null
        if (data) {
            localStorage.token = data.token
            HTTP.defaults.headers.common['Authorization'] = 'Bearer ' + data.token
            router.push({name: 'AdminMain'})
            // location.reload()
        } else {
            if (localStorage.token) {
                HTTP.defaults.headers.common['Authorization'] = 'Bearer ' + localStorage.token
                Auth.me(data => { user = data }, () => {
                    localStorage.removeItem('token')
                    commit('LOG_OUT')
                })
                setTimeout(() => {
                    commit('LOG_IN', user)
                }, 1000)
            }
        }
    },
    REMOVE_USER: ({commit}) => {
        localStorage.removeItem('token')
        commit('LOG_OUT')
        router.push({name: 'Auth'})
    }
}

export default {
    state,
    getters,
    actions,
    mutations
}
