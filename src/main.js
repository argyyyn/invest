import Vue from 'vue'
import App from './App.vue'
import './registerServiceWorker'
import 'roboto-fontface'
import 'jquery'
import 'bootstrap/dist/css/bootstrap.min.css'
import 'bootstrap/dist/js/bootstrap.min'
import 'font-awesome/css/font-awesome.css'
import router from './router'
import {store} from './store'
import './sass/main.sass'


import PictureInput from 'vue-picture-input'
import End from '@/components/End.vue'
import Start from '@/components/Start.vue'
import Locale from '@/components/Locale.vue'
import {sync} from "vuex-router-sync";
import Notifications from 'vue-notification'
Vue.use(Notifications)
Vue.component('end', End)
Vue.component('start', Start)
Vue.component('picture-input', PictureInput)
Vue.component('locale', Locale)

Vue.config.productionTip = false

if (localStorage.token) {
  store.dispatch('SET_USER')
}
sync(store, router)

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
